defmodule Day08 do
  @moduledoc """
  Documentation for `Day08`.
  """

  def parse_line(line) do
    line |> String.replace("+", "") |> String.split(" ")
  end

  def solve_ex1(input) do
    code = Enum.map(input, &parse_line(&1))
    while_1(0, code) |> IO.inspect()
  end

  def while_1(index, code) do
    code_line = Enum.at(code, index)

    if stop?(code_line) do
      0
    else
      acc?(code_line) + while_1(index + index?(code_line), code |> List.replace_at(index, ""))
    end
  end

  def acc?(["acc", x]), do: String.to_integer(x)
  def acc?(_), do: 0
  def index?(["jmp", x]), do: String.to_integer(x)
  def index?(["nop", _]), do: 1
  def index?(["acc", _]), do: 1
  def stop?(""), do: true
  def stop?(_), do: false

  def solve_ex2(input) do
    code = Enum.map(input, &parse_line(&1))

    Enum.find_value(0..(Enum.count(code) - 1), fn index ->
      [instr, _] = Enum.at(code, index)

      if instr != "acc" do
        solution = while_2(0, replace(code, index))
        if elem(solution, 1) == true, do: elem(solution, 0), else: false
      else
        false
      end
    end)
  end

  def replace(code, index) do
    [instr, nr] = Enum.at(code, index)
    List.replace_at(code, index, [switch(instr), nr])
  end

  def switch("jmp"), do: "nop"
  def switch("nop"), do: "jmp"

  def while_2(index, code) do
    if index == Enum.count(code) do
      {0, true}
    else
      code_line = Enum.at(code, index)

      if stop?(code_line) do
        {-1, false}
      else
        next = while_2(index + index?(code_line), code |> List.replace_at(index, ""))
        {acc?(code_line) + elem(next, 0), elem(next, 1)}
      end
    end
  end

  #### Clutter
  @doc """
  Ex1:

  ## Examples
      iex> Day08.ex1()
      1867
  """
  def ex1() do
    Shared.read_input_of_day_to_list("08")
    |> ex1
  end

  def ex1(input) do
    solve_ex1(input)
  end

  @doc """
  Ex2:

  ## Examples
      iex> Day08.ex2()
      1303
  """
  def ex2() do
    Shared.read_input_of_day_to_list("08")
    |> ex2
  end

  def ex2(input) do
    solve_ex2(input)
  end

  def main do
  end
end
