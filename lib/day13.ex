defmodule Day13 do
  @moduledoc """
  Documentation for `Day13`.
  """

  def parse_input(input) do
    {Enum.at(input, 0) |> String.to_integer(), String.split(Enum.at(input, 1), ",")}
  end

  def solve_ex1(input) do
    {time, schedule} = parse_input(input)

    schedule
    |> Enum.reject(&(&1 == "x"))
    |> Enum.map(&String.to_integer(&1))
    |> Enum.map(&{&1 - rem(time, &1), &1})
    |> Enum.min_by(&elem(&1, 0))
    |> (&(elem(&1, 0) * elem(&1, 1))).()
  end

  def solve_ex2(input) do
    {_, schedule} = parse_input(input)

    schedule =
      Enum.zip(schedule, 0..Enum.count(schedule))
      |> Enum.reject(&(elem(&1, 0) == "x"))
      |> Enum.map(&{String.to_integer(elem(&1, 0)), elem(&1, 1)})
      |> Enum.sort_by(&elem(&1, 0), :desc)
      |> IO.inspect()

    # brute force takes too long
    # cheated to get initial index D:
    while(schedule, 537_619_546_700)
  end

  def while(schedule, index) do
    {x, offset} = Enum.at(schedule, 0)
    depart_time = x * index
    t = depart_time - offset

    solution? =
      Enum.find(schedule, fn {x, offset} ->
        !(rem(t + offset, x) == 0)
      end)
      |> is_nil()

    if solution?, do: t, else: while(schedule, index + 1)
  end

  #### Clutter
  @doc """
  Ex1:

  ## Examples
      iex> Day13.ex1()
      3865
  """
  def ex1() do
    Shared.read_input_of_day_to_list("13")
    |> ex1
  end

  def ex1(input) do
    solve_ex1(input)
  end

  @doc """
  Ex2:

  ## Examples
      iex> Day13.ex2()
      415579909629976
  """
  def ex2() do
    Shared.read_input_of_day_to_list("13")
    |> ex2
  end

  def ex2(input) do
    solve_ex2(input)
  end

  def measure(function) do
    function
    |> :timer.tc()
    |> elem(0)
    |> Kernel./(1_000_000)
  end

  def main() do
    measure(&ex1/0) |> IO.inspect()
    measure(&ex2/0) |> IO.inspect()
  end
end
