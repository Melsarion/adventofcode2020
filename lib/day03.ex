defmodule Day03 do
  @moduledoc """
  Documentation for `Day03`.
  """

  def tree?("#"), do: 1
  def tree?("."), do: 0

  def find_trees(input, right, down) do
    input
    |> Enum.drop(down)
    |> Enum.take_every(down)
    |> Enum.reduce({0, 0}, fn row, {trees, index} ->
      index = rem(index + right, String.length(row))
      {trees + tree?(String.at(row, index)), index}
    end)
    |> elem(0)
  end

  def solve_ex1(input) do
    find_trees(input, 3, 1)
  end

  def solve_ex2(input) do
    [{1, 1}, {3, 1}, {5, 1}, {7, 1}, {1, 2}]
    |> Enum.reduce(1, fn {right, down}, acc ->
      acc * find_trees(input, right, down)
    end)
  end

  #### Clutter

  @doc """
  Ex1:

  ## Examples
      iex> Day03.ex1()
      171
  """
  def ex1() do
    Shared.read_input_of_day_to_list("03")
    |> ex1
  end

  def ex1(input) do
    solve_ex1(input)
  end

  @doc """
  Ex2:

  ## Examples
      iex> Day03.ex2()
      699
  """
  def ex2() do
    Shared.read_input_of_day_to_list("03")
    |> ex2
  end

  def ex2(input) do
    solve_ex2(input)
  end

  def main do
  end
end
