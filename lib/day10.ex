defmodule Day10 do
  @moduledoc """
  Documentation for `Day10`.
  """

  def solve_ex1(input) do
    input_sorted = Enum.sort(input)

    ([0] ++ input_sorted ++ [List.last(input_sorted) + 3])
    |> Enum.chunk_every(2, 1, :discard)
    |> Enum.reduce({0, 0}, fn pair, {diff1, diff3} ->
      {diff1 + diff1?(pair), diff3 + diff3?(pair)}
    end)
    |> (&(elem(&1, 0) * elem(&1, 1))).()
    |> IO.inspect()
  end

  def diff1?([a, b]) when b - a == 1, do: 1
  def diff1?(_), do: 0
  def diff3?([a, b]) when b - a == 3, do: 1
  def diff3?(_), do: 0

  # gebruik de volledige gesorteerde lijst, en kijk welke vastzitten
  # namelijk sprong van 3 of sprong van 2-2 (middelste dan vast)
  # splits lijst in die subprobleempjes en doe daar bruteforce op
  def solve_ex2(input) do
    input_sorted = [0] ++ Enum.sort(input) ++ [Enum.max(input) + 3]
    splitters = find_splitters(input_sorted, 1) |> Enum.sort()

    Enum.chunk_every([0] ++ splitters ++ [List.last(input_sorted)], 2, 1, :discard)
    |> Enum.reject(&(Enum.at(&1, 0) == Enum.at(&1, 1)))
    # fill lists with values
    |> Enum.map(fn [min, max] -> Enum.filter(input_sorted, fn x -> x in min..max end) end)
    |> Enum.reduce(1, fn list, acc ->
      acc * splitting(list)
    end)
  end

  def splitting(list) do
    # Algoritm:
    # find all 3 jumps (split normally) and 2x2 jumps (split with duplicate of middle number) -> VIA SPLITTERS
    # for every list X each other
    # in list itself + the choices
    # # 1jump = start without number, or no start with number
    # # 2 jump= without the start, yes/no number
    # # count = 3 -> ANSWER 2
    # # count = 4 -> if 2-jump ANSWER 2, else ANSWER 3
    out =
      case Enum.count(list) do
        2 ->
          1

        3 ->
          2

        4 ->
          # No 2jump, yes 2jump
          if List.last(list) - List.first(list) == 3, do: 4, else: 3

        _ ->
          {start, second} = {Enum.at(list, 0), Enum.at(list, 1)}

          if second - start == 1 do
            splitting(List.delete(list, second)) + splitting(List.delete(list, start))
          else
            splitting(List.delete(list, start) |> List.delete(second)) +
              splitting(List.delete(list, start))
          end
      end

    out
  end

  def find_splitters(list, index_current) do
    if Enum.count(list) - 1 < index_current + 1 do
      []
    else
      {next, current} = {Enum.at(list, index_current + 1), Enum.at(list, index_current)}

      if next - current == 3 do
        [current, next] ++ find_splitters(list, index_current + 1)
      else
        previous = Enum.at(list, index_current - 1)

        if next - current == 2 and current - previous == 2 do
          [current, current] ++ find_splitters(list, index_current + 1)
        else
          find_splitters(list, index_current + 1)
        end
      end
    end
  end

  #### Clutter
  @doc """
  Ex1:

  ## Examples
      iex> Day10.ex1()
      2263
  """
  def ex1() do
    Shared.read_input_of_day_to_int_list("10")
    |> ex1
  end

  def ex1(input) do
    solve_ex1(input)
  end

  @doc """
  Ex2:

  ## Examples
      iex> Day10.ex2()
      396857386627072
  """
  def ex2() do
    Shared.read_input_of_day_to_int_list("10")
    |> ex2
  end

  def ex2(input) do
    solve_ex2(input)
  end

  def measure(function) do
    function
    |> :timer.tc()
    |> elem(0)
    |> Kernel./(1_000_000)
  end

  def main() do
    measure(&ex2/0) |> IO.inspect()
  end
end
