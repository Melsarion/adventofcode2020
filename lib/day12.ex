defmodule Day12 do
  @moduledoc """
  Documentation for `Day12`.
  """

  # coord = {x,y,dir}
  def solve_ex1(input) do
    {x, y, dir} = move_ship({0, 0, 0}, input)
    abs(x) + abs(y)
  end

  def move_ship(start, instructions) do
    instructions
    |> Enum.reduce(start, fn instr, coord ->
      {a, b} = String.split_at(instr, 1)
      instr(a, String.to_integer(b), coord)
    end)
  end

  def dir_to_coords(dir) do
    [{1, 0}, {0, -1}, {-1, 0}, {0, 1}]
    |> Enum.at(rem(dir, 4))
  end

  def instr("L", degr, {x, y, dir}), do: {x, y, dir + div(-degr, 90)}
  def instr("R", degr, {x, y, dir}), do: {x, y, dir + div(degr, 90)}
  def instr("N", nr, {x, y, dir}), do: {x, y + nr, dir}
  def instr("S", nr, {x, y, dir}), do: {x, y - nr, dir}
  def instr("E", nr, {x, y, dir}), do: {x + nr, y, dir}
  def instr("W", nr, {x, y, dir}), do: {x - nr, y, dir}

  def instr("F", nr, {x, y, dir}),
    do: {x + elem(dir_to_coords(dir), 0) * nr, y + elem(dir_to_coords(dir), 1) * nr, dir}

  def rotate_wp({x, y, wpx, wpy}, degr) do
    {cos, sin} = [{1, 0}, {0, 1}, {-1, 0}, {0, -1}] |> Enum.at(div(degr, 90))
    wpxx = wpx * cos - wpy * sin
    wpyy = wpx * sin + wpy * cos
    {x, y, wpxx, wpyy}
  end

  def instr_wp("L", degr, {x, y, wpx, wpy}), do: rotate_wp({x, y, wpx, wpy}, degr)
  def instr_wp("R", degr, {x, y, wpx, wpy}), do: rotate_wp({x, y, wpx, wpy}, -degr)
  def instr_wp("N", nr, {x, y, wpx, wpy}), do: {x, y, wpx, wpy + nr}
  def instr_wp("S", nr, {x, y, wpx, wpy}), do: {x, y, wpx, wpy - nr}
  def instr_wp("E", nr, {x, y, wpx, wpy}), do: {x, y, wpx + nr, wpy}
  def instr_wp("W", nr, {x, y, wpx, wpy}), do: {x, y, wpx - nr, wpy}
  def instr_wp("F", nr, {x, y, wpx, wpy}), do: {x + nr * wpx, y + nr * wpy, wpx, wpy}

  # coord = {x,y,dir, wpx, wpy}
  def move_ship_waypoint(start, instructions) do
    instructions
    |> Enum.reduce(start, fn instr, coord ->
      {a, b} = String.split_at(instr, 1) |> IO.inspect()
      instr_wp(a, String.to_integer(b), coord) |> IO.inspect()
    end)
  end

  def solve_ex2(input) do
    {x, y, wpx, wpy} = move_ship_waypoint({0, 0, 10, 1}, input)
    abs(x) + abs(y)
  end

  #### Clutter
  @doc """
  Ex1:

  ## Examples
      iex> Day12.ex1()
      2228
  """
  def ex1() do
    Shared.read_input_of_day_to_list("12")
    |> ex1
  end

  def ex1(input) do
    solve_ex1(input)
  end

  @doc """
  Ex2:

  ## Examples
      iex> Day12.ex2()
      396857386627072
  """
  def ex2() do
    Shared.read_input_of_day_to_list("12")
    |> ex2
  end

  def ex2(input) do
    solve_ex2(input)
  end

  def measure(function) do
    function
    |> :timer.tc()
    |> elem(0)
    |> Kernel./(1_000_000)
  end

  def main() do
    measure(&ex1/0) |> IO.inspect()
    measure(&ex2/0) |> IO.inspect()
  end
end
