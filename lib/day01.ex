defmodule Day01 do
  @moduledoc """
  Documentation for `Day01`.
  """

  def solve_ex1(input) do
    _product = while(Enum.sort(input), 0, Enum.count(input) - 1, 0)
  end

  def while(list, min_index, max_index, 2020) do
    Enum.at(list, min_index) * Enum.at(list, max_index)
  end

  def while(list, min_index, max_index, _sum) do
    sum = Enum.at(list, min_index) + Enum.at(list, max_index)
    while(list, min_index + min(sum), max_index + max(sum), sum)
  end

  def min(sum) when sum < 2020, do: +1
  def min(sum) when sum >= 2020, do: 0
  def max(sum) when sum <= 2020, do: 0
  def max(sum) when sum > 2020, do: -1

  def solve_ex2(input) do
    _product = bruteforce(input)
  end

  def bruteforce(input) do
    Enum.find_value(input, fn x ->
      Enum.find_value(input, fn y ->
        Enum.find_value(input, fn z -> if x + y + z == 2020, do: x * y * z end)
      end)
    end)
  end

  @doc """
  Ex1:

  ## Examples

      iex> Day01.ex1([1721,979,366,299,675,1456])
      514579

      iex> Day01.ex1()
      252724
  """
  def ex1() do
    Shared.read_input_of_day_to_int_list("01")
    |> ex1
  end

  def ex1(input) do
    solve_ex1(input)
  end

  @doc """
  Ex2:

  ## Examples
      iex> Day01.ex2()
      276912720
  """
  def ex2() do
    Shared.read_input_of_day_to_int_list("01")
    |> ex2
  end

  def ex2(input) do
    solve_ex2(input)
  end

  def main do
  end
end
