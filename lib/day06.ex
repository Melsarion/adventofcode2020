defmodule Day06 do
  @moduledoc """
  Documentation for `Day06`.
  """

  def count_unique_codepoints(group) do
    group
    |> String.replace("\n", "")
    |> String.codepoints()
    |> Enum.uniq()
    |> Enum.count()
  end

  def count_duplicate_codepoints(group) do
    group_split = String.replace(group, "\n", "")
    groups = String.length(group) - String.length(group_split) + 1

    group_split
    |> String.codepoints()
    |> Enum.frequencies()
    |> Map.to_list()
    |> Enum.filter(&(elem(&1, 1) == groups))
    |> Enum.count()
  end

  def solve_ex1(input) do
    input
    |> Enum.map(&count_unique_codepoints(&1))
    |> Enum.sum()
  end

  def solve_ex2(input) do
    input
    |> Enum.map(&count_duplicate_codepoints(&1))
    |> Enum.sum()
  end

  #### Clutter
  @doc """
  Ex1:

  ## Examples
      iex> Day06.ex1()
      6259
  """
  def ex1() do
    Shared.read_input_of_day_to_list("06", "\n\n")
    |> ex1
  end

  def ex1(input) do
    solve_ex1(input)
  end

  @doc """
  Ex2:

  ## Examples
      iex> Day06.ex2()
      522
  """
  def ex2() do
    Shared.read_input_of_day_to_list("06", "\n\n")
    |> ex2
  end

  def ex2(input) do
    solve_ex2(input)
  end

  def main do
  end
end
