defmodule Aoc2020 do
  @moduledoc """
  Documentation for `Aoc2020`.
  """

  @doc """
  Ex1:

  ## Examples

      iex> Aoc2020.ex1()
      "hello world"

  """
  def ex1 do
    Shared.read_input_of_day("0")
  end

  @doc """
  Ex2:

  ## Examples

      iex> Aoc2020.ex2()
      "hello world"

  """
  def ex2 do
    Shared.read_input_of_day("0")
  end
end
