defmodule Day11 do
  @moduledoc """
  Documentation for `Day11`.
  """

  def surround_with_points(matrix) do
    matrix = Enum.map(matrix, fn row -> ["."] ++ row ++ ["."] end)
    columns = Enum.at(matrix, 0) |> Enum.count()
    _matrix = [List.duplicate(".", columns)] ++ matrix ++ [List.duplicate(".", columns)]
  end

  def while(matrix, columns, rows, fun, occup_limit) do
    new_matrix =
      update(matrix, 0, columns - 2, (columns - 2) * (rows - 2), fun, occup_limit)
      |> Enum.chunk_every(columns - 2)
      |> surround_with_points

    if copy?(matrix, new_matrix) do
      new_matrix
    else
      while(new_matrix, columns, rows, fun, occup_limit)
    end
  end

  def copy?(matrix1, matrix2) do
    matrix1 = List.flatten(matrix1)
    matrix2 = List.flatten(matrix2)
    zipped = Enum.zip(matrix1, matrix2)
    count = Enum.filter(zipped, fn {m1, m2} -> m1 != m2 end) |> Enum.count()
    count == 0
  end

  def elem(matrix, row, column) do
    if Enum.count(matrix) > row + 1 and row + 1 >= 0 do
      row = Enum.at(matrix, row + 1)

      if Enum.count(row) > column + 1 and column + 1 >= 0 do
        Enum.at(row, column + 1)
      else
        -1
      end
    else
      -1
    end
  end

  def update(matrix, nr, mod, max, fun, occup_limit) do
    column = rem(nr, mod)
    row = div(nr - column, mod)
    %{seats: _seats, occup: occup, points: _points} = fun.(matrix, row, column)

    element =
      cond do
        elem(matrix, row, column) == "L" and occup == 0 ->
          ["#"]

        elem(matrix, row, column) == "#" and occup >= occup_limit ->
          ["L"]

        true ->
          [elem(matrix, row, column)]
      end

    if nr < max - 1 do
      element ++ update(matrix, nr + 1, mod, max, fun, occup_limit)
    else
      element
    end
  end

  def surround(matrix, row, column) do
    surr = [
      elem(matrix, row - 1, column - 1),
      elem(matrix, row - 1, column),
      elem(matrix, row - 1, column + 1),
      elem(matrix, row, column - 1),
      elem(matrix, row, column + 1),
      elem(matrix, row + 1, column - 1),
      elem(matrix, row + 1, column),
      elem(matrix, row + 1, column + 1)
    ]

    points = Enum.count(surr, &(&1 == "."))
    seats = Enum.count(surr, &(&1 == "L"))
    occup = Enum.count(surr, &(&1 == "#"))
    %{points: points, seats: seats, occup: occup}
  end

  def find_non_point(matrix, row, column, direction_row, direction_column) do
    e = elem(matrix, row + direction_row, column + direction_column)

    case e do
      "#" ->
        "#"

      "L" ->
        "L"

      "." ->
        find_non_point(
          matrix,
          row + direction_row,
          column + direction_column,
          direction_row,
          direction_column
        )

      -1 ->
        "."

      nil ->
        "."
    end
  end

  def surround2(matrix, row, column) do
    surr = [
      find_non_point(matrix, row, column, -1, -1),
      find_non_point(matrix, row, column, -1, 0),
      find_non_point(matrix, row, column, -1, 1),
      find_non_point(matrix, row, column, 0, -1),
      find_non_point(matrix, row, column, 0, 1),
      find_non_point(matrix, row, column, 1, -1),
      find_non_point(matrix, row, column, 1, 0),
      find_non_point(matrix, row, column, 1, 1)
    ]

    points = Enum.count(surr, &(&1 == "."))
    seats = Enum.count(surr, &(&1 == "L"))
    occup = Enum.count(surr, &(&1 == "#"))
    %{points: points, seats: seats, occup: occup}
  end

  def solve_ex1(input) do
    input = Enum.map(input, fn row -> String.codepoints(row) end) |> surround_with_points()
    rows = Enum.count(input)
    columns = Enum.at(input, 0) |> Enum.count()
    end_matrix = while(input, columns, rows, &surround/3, 4)
    Enum.count(List.flatten(end_matrix), fn x -> x == "#" end)
  end

  def solve_ex2(input) do
    input = Enum.map(input, fn row -> String.codepoints(row) end) |> surround_with_points()
    rows = Enum.count(input)
    columns = Enum.at(input, 0) |> Enum.count()
    end_matrix = while(input, columns, rows, &surround2/3, 5) |> IO.inspect()
    Enum.count(List.flatten(end_matrix), fn x -> x == "#" end)
  end

  #### Clutter
  @doc """
  Ex1:

  ## Examples
      iex> Day11.ex1()
      2238
  """
  def ex1() do
    Shared.read_input_of_day_to_list("11")
    |> ex1
  end

  def ex1(input) do
    solve_ex1(input)
  end

  @doc """
  Ex2:

  ## Examples
      iex> Day11.ex2()
      396857386627072
  """
  def ex2() do
    Shared.read_input_of_day_to_list("11")
    |> ex2
  end

  def ex2(input) do
    solve_ex2(input)
  end

  def measure(function) do
    function
    |> :timer.tc()
    |> elem(0)
    |> Kernel./(1_000_000)
  end

  def main() do
    measure(&ex1/0) |> IO.inspect()
    measure(&ex2/0) |> IO.inspect()
  end
end
