defmodule Shared do
  @moduledoc false

  def read_input_of_day(day) do
    Path.join(:code.priv_dir(:aoc2020), "input_#{day}.txt")
    |> File.read!()
    #    |> IEx.Info.info
    |> String.trim()
  end

  def read_input_of_day_to_int_list(day) do
    read_input_of_day_to_list(day)
    |> Enum.map(fn x -> String.to_integer(x) end)
  end

  def read_input_of_day_to_list(day, splitter \\ "\n") do
    Path.join(:code.priv_dir(:aoc2020), "input_#{day}.txt")
    |> File.read!()
    |> String.trim()
    |> String.split(splitter)
  end
end
