defmodule Day04 do
  @moduledoc """
  Documentation for `Day04`.
  """
  def passwords_to_list(input) do
    input
    |> Enum.map(fn line ->
      ~w(#{String.replace(line, "\n", " ")}) |> Enum.map(fn field -> String.split(field, ":") end)
    end)
  end

  def solve_ex1(input) do
    input
    |> passwords_to_list()
    |> Enum.count(fn word_list ->
      Enum.filter(word_list, &(List.first(&1) in ~w(byr iyr eyr hgt hcl ecl pid)))
      |> Enum.count()
      |> (&(&1 == 7)).()
    end)
  end

  def solve_ex2(input) do
    input
    |> passwords_to_list()
    |> Enum.count(fn word_list ->
      word_list
      |> Enum.filter(&validate(&1))
      |> Enum.count()
      |> (&(&1 == 7)).()
    end)
  end

  def validate(["byr", date]), do: String.to_integer(date) in 1920..2002
  def validate(["iyr", date]), do: String.to_integer(date) in 2010..2020
  def validate(["eyr", date]), do: String.to_integer(date) in 2020..2030
  def validate(["hgt", height]), do: validate_height(String.split_at(height, -2))
  def validate(["hcl", color]), do: Regex.match?(~r/^#([0-9]|[a-f]){6}$/, color)
  def validate(["ecl", color]), do: color in ~w(amb blu brn gry grn hzl oth)
  def validate(["pid", code]), do: Regex.match?(~r/^[0-9]{9}$/, code)
  def validate([_, _]), do: false
  def validate_height({num, "cm"}), do: String.to_integer(num) in 150..193
  def validate_height({num, "in"}), do: String.to_integer(num) in 59..76
  def validate_height({_, _}), do: false
  #### Clutter

  @doc """
  Ex1:

  ## Examples
      iex> Day04.ex1()
      200
  """
  def ex1() do
    Shared.read_input_of_day_to_list("04", "\n\n")
    |> ex1
  end

  def ex1(input) do
    solve_ex1(input)
  end

  @doc """
  Ex2:

  ## Examples
      iex> Day04.ex2()
      699
  """
  def ex2() do
    Shared.read_input_of_day_to_list("04", "\n\n")
    |> ex2
  end

  def ex2(input) do
    solve_ex2(input)
  end

  def main do
  end
end
