defmodule Day20 do
  @moduledoc """
  Documentation for `Day20`.
  """
  def parse_input(input) do
    input
    |> Enum.chunk_every(12, 12)
    |> Enum.map(fn chunk ->
      nr = Enum.at(chunk, 0) |> String.slice(5,4) |> String.to_integer()
      top = Enum.at(chunk,1)
      bottom = Enum.at(chunk, 10)
      left = chunk |> List.delete_at(0) |> List.delete_at(10) |> Enum.reduce("", fn row, acc -> acc <> String.first(row) end)
      right = chunk |> List.delete_at(0) |> List.delete_at(10) |> Enum.reduce("", fn row, acc -> acc <> String.last(row) end)
      [{:normal, left, nr}, {:reversed,String.reverse(left), nr},
        {:normal,top, nr}, {:reversed,String.reverse(top), nr},
        {:normal,bottom, nr}, {:reversed,String.reverse(bottom), nr},
        {:normal,right, nr}, {:reversed,String.reverse(right), nr},
      ]
    end)
    |> List.flatten()
    |> Enum.reduce(%{}, fn {reversed?, str, nr}, acc ->
      if Map.has_key?(acc, str) do
        Map.update(acc, str, [], &([nr] ++ &1))
      else
        Map.update(acc, String.reverse(str), [], &([nr] ++ &1))
      end
    end)
    |> Enum.map(fn {str,list} -> {str, Enum.uniq(list)} end)
  end
  def solve_ex1(input) do
    parse_input(input)
    |> Enum.map(&elem(&1,1))
    |> Enum.filter(fn list -> Enum.count(list) == 1 end)
    |> List.flatten()
    |> Enum.sort()
    |> Enum.chunk_by(fn x -> x end)
    |> Enum.map(fn list -> {Enum.at(list, 0), Enum.count(list)} end)
    |> Enum.filter(fn {nr, count} -> count == 2 end)
    |> Enum.reduce(1, fn {nr, count}, acc -> acc *nr end)
  end

  def solve_ex2(input) do

  end

  #### Clutter
  @doc """
  Ex1:

  ## Examples
      iex> Day20.ex1()
      3865
  """
  def ex1() do
    Shared.read_input_of_day_to_list("20")
    |> ex1
  end

  def ex1(input) do
    solve_ex1(input)
  end

  @doc """
  Ex2:

  ## Examples
      iex> Day20.ex2()
      415579909629976
  """
  def ex2() do
    Shared.read_input_of_day_to_list("20")
    |> ex2
  end

  def ex2(input) do
    solve_ex2(input)
  end

  def measure(function) do
    function
    |> :timer.tc()
    |> elem(0)
    |> Kernel./(1_000_000)
  end

  def main() do
    measure(&ex1/0) |> IO.inspect()
    measure(&ex2/0) |> IO.inspect()
  end
end
