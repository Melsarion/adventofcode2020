defmodule Day09 do
  @moduledoc """
  Documentation for `Day09`.
  """

  def solve_ex1(input) do
    pre_size = 25

    index_solution =
      Enum.find(0..(Enum.count(input) - pre_size), fn index ->
        !sum?(input, pre_size, index)
      end)

    Enum.at(input, index_solution + pre_size)
  end

  def sum?(input, pre_size, index) do
    preamble = Enum.slice(input, index, pre_size)
    nr = Enum.at(input, index + pre_size)

    out =
      Enum.find(preamble, fn nr1 ->
        Enum.find(preamble, fn nr2 ->
          nr1 + nr2 == nr and nr1 != nr2
        end)
      end)

    !is_nil(out)
  end

  def solve_ex2(input) do
    find = 90_433_990
    {start, stop} = while(find, List.delete(input, find), 0, 1)
    {min, max} = Enum.min_max(Enum.slice(input, start, stop - start))
    min + max
  end

  def while(find, list, start, stop) do
    case Enum.sum(Enum.slice(list, start, stop - start)) do
      x when x == find -> {start, stop}
      x when x < find -> while(find, list, start, stop + 1)
      x when x > find -> while(find, list, start + 1, stop)
    end
  end

  #### Clutter
  @doc """
  Ex1:

  ## Examples
      iex> Day09.ex1()
      90433990
  """
  def ex1() do
    Shared.read_input_of_day_to_int_list("09")
    |> ex1
  end

  def ex1(input) do
    solve_ex1(input)
  end

  @doc """
  Ex2:

  ## Examples
      iex> Day09.ex2()
      11691646
  """
  def ex2() do
    Shared.read_input_of_day_to_int_list("09")
    |> ex2
  end

  def ex2(input) do
    solve_ex2(input)
  end

  def main do
  end
end
