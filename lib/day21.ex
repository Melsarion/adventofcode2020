defmodule Day21 do
  @moduledoc """
  Documentation for `21`.
  """
  def parse_input(input) do
    input
    |> Enum.map(fn str ->
      str
      |> String.replace_trailing(")", "")
      |> String.split([" (contains "])
      |> Enum.map(fn str -> String.split(str, [", ", " "]) end)
    end)
  end

  def ingredients(parsed_input) do
    parsed_input
    |> Enum.map(fn line -> Enum.at(line, 1) end)
    |> List.flatten()
    |> Enum.uniq()
  end

  def solve_ex1(input) do
    input = parse_input(input)
    ingredients = ingredients(input)

    foods = elem(reduce_ingredients(input, ingredients, 0), 0)
    |> List.flatten()
    |> Enum.uniq()

    input
    |> Enum.map(fn [foods1, ingr] -> foods1 end)
    |> List.flatten()
    |> Enum.filter(fn one_food -> one_food in foods end)
    |> Enum.count
  end

  def reduce_ingredients(input, ingredients, index, solutions \\ []) do
    if ingredients == [] do
      {input, solutions}
    else
      ingr = Enum.at(ingredients, rem(index, Enum.count(ingredients)))
      match? = find_match(input, ingr)

      if match? == [] or Enum.count(match?) > 1 do
        reduce_ingredients(input, ingredients, index + 1, solutions)
      else
        input =
          input
          |> Enum.map(fn [foods, ingredients] ->
            [
              Enum.reject(foods, fn food1 -> food1 == List.first(match?) end),
              Enum.reject(ingredients, fn ingr1 -> ingr1 == ingr end)
            ]
          end)

        ingredients = Enum.reject(ingredients, fn ingr2 -> ingr2 == ingr end)
        solutions = solutions ++ [{match?, ingr}]
        reduce_ingredients(input, ingredients, index + 1, solutions)
      end
    end
  end

  def find_match(input, ingr) do
    input
    |> Enum.filter(fn [foods, ingredients] -> ingr in ingredients end)
    |> Enum.reduce(:empty, fn [foods, ingredients], acc ->
      if acc == :empty do
        foods
      else
        _same_food = foods |> Enum.filter(fn food -> food in acc end)
      end
    end)
  end

  def solve_ex2(input) do
    input = parse_input(input)
    ingredients = ingredients(input)

    solutions = elem(reduce_ingredients(input, ingredients, 0), 1)
    solutions
    |> Enum.sort_by(&elem(&1,1))
    |> Enum.reduce("",fn {[food], ingr},acc ->
      acc <> "," <> food
    end)
    |> String.replace_prefix(",","")
  end

  #### Clutter
  @doc """
  Ex1:

  ## Examples
      iex> Day21.ex1()
      2556
  """
  def ex1() do
    Shared.read_input_of_day_to_list("21")
    |> ex1
  end

  def ex1(input) do
    solve_ex1(input)
  end

  @doc """
  Ex2:

  ## Examples
      iex> Day21.ex2()
      415579909629976
  """
  def ex2() do
    Shared.read_input_of_day_to_list("21")
    |> ex2
  end

  def ex2(input) do
    solve_ex2(input)
  end

  def measure(function) do
    function
    |> :timer.tc()
    |> elem(0)
    |> Kernel./(1_000_000)
  end

  def main() do
    measure(&ex1/0) |> IO.inspect()
    measure(&ex2/0) |> IO.inspect()
  end
end
