defmodule Day02 do
  @moduledoc """
  Documentation for `Day02`.
  """

  def input_preprocessing(input) do
    input
    |> Enum.map(fn x ->
      # string processing
      [policy, pass] = String.split(x, ": ")
      pass = pass |> to_charlist()
      [minmax, char] = policy |> String.split(" ")
      char = to_charlist(char) |> Enum.at(0)
      [min, max] = minmax |> String.split("-") |> Enum.map(&String.to_integer(&1))
      {min, max, char, pass}
    end)
  end

  def solve_ex1(input) do
    input
    |> input_preprocessing()
    |> Enum.count(fn {min, max, char, pass} ->
      count = pass |> Enum.count(&(&1 == char))
      count >= min and count <= max
    end)
  end

  def solve_ex2(input) do
    input
    |> input_preprocessing()
    |> Enum.count(fn {min, max, char, pass} ->
      Enum.at(pass, min - 1) == char != (Enum.at(pass, max - 1) == char)
    end)
  end

  #### Clutter

  @doc """
  Ex1:

  ## Examples
      iex> Day02.ex1()
      638
  """
  def ex1() do
    Shared.read_input_of_day_to_list("02")
    |> ex1
  end

  def ex1(input) do
    solve_ex1(input)
  end

  @doc """
  Ex2:

  ## Examples
      iex> Day02.ex2()
      699
  """
  def ex2() do
    Shared.read_input_of_day_to_list("02")
    |> ex2
  end

  def ex2(input) do
    solve_ex2(input)
  end

  def main do
  end
end
