defmodule Day23 do
  @moduledoc """
  Documentation for `23`.
  """
  def parse_input(input) do
    input
    |> String.codepoints()
    |> Enum.map(&String.to_integer(&1))
  end

  def solve_ex1(input) do
    input = parse_input(input)
    list = input
    current = List.first(list)
    list =
      list
      |> Enum.chunk_every(2, 1, :discard)
      |> Enum.map(fn [x, y] -> {x, y} end)
      |> Map.new()
      |> Map.put(8,4)
      |> IO.inspect
    destination = current - 1
    out = play_cup_fast(list, current, destination, 1, 100, 9)

    IO.inspect(out)
  end

  def play_cup(list, current_cup, destination, turn, turns, max) do
    if rem(turn, 10) == 0, do: IO.inspect(turn)
    current_cup_index = Enum.find_index(list, fn x -> x == current_cup end)
    pick_up = Enum.slice(list ++ list, current_cup_index + 1, 3)

    if turn > turns do
      list
    else
      if destination in pick_up or destination <= 0 do
        dest = if destination - 1 <= 0, do: max, else: destination - 1
        play_cup(list, current_cup, dest, turn, turns, max)
      else
        circle = Enum.reject(list, fn x -> x in pick_up end)
        dest_index = Enum.find_index(circle, fn x -> x == destination end)

        out =
          Enum.slice(circle, 0..dest_index) ++
            pick_up ++ Enum.slice(circle, (dest_index + 1)..Enum.count(circle))

        current_cup_index_out = Enum.find_index(out, fn x -> x == current_cup end)
        next_current_cup = Enum.at(out ++ out, current_cup_index_out + 1)

        next_dest = next_current_cup - 1
        play_cup(out, next_current_cup, next_dest, turn + 1, turns, max)
      end
    end
  end

  def play_cup_fast(list, current_cup, destination, turn, turns, max) do
    if rem(turn, 1000000) == 0, do: IO.inspect(turn)
    a = Map.get(list, current_cup)
    b = Map.get(list, a)
    c = Map.get(list, b)
    after_c = Map.get(list, c)
    pick_up = [a, b, c]
    if turn > turns do
      list
    else
      if destination in pick_up or destination <= 0 do
        dest = if destination - 1 <= 0, do: max, else: destination - 1
        play_cup_fast(list, current_cup, dest, turn, turns, max)
      else
        after_destination = Map.get(list, destination)
        out = list |> Map.put(destination, a) |> Map.put(c, after_destination) |> Map.put(current_cup, after_c)

        next_current_cup = Map.get(out, current_cup)

        next_dest = next_current_cup - 1
        play_cup_fast(out, next_current_cup, next_dest, turn + 1, turns, max)
      end
    end
  end

  def solve_ex2(input) do
    input_list = parse_input(input)

    input =
      input_list
      |> Enum.chunk_every(2, 1, :discard)
      |> Enum.map(fn [x, y] -> {x, y} end)

    extra =
      Enum.map(10..1_000_000, fn x -> x end)
      |> Enum.chunk_every(2, 1,:discard)
      |> Enum.map(fn [x, y] -> {x, y} end)

    list = Map.merge(Enum.into(input, %{}), Enum.into(extra, %{}))
    list = list |> Map.put(8, 10) |> Map.put(1000000,4)
    current = List.first(input_list)
    destination = current - 1
    out = play_cup_fast(list, current, destination, 1, 10_000_000, 1_000_000)
    a = Map.get(out, 1)
    b = Map.get(out, a)
    a * b
  end

  #### Clutter
  @doc """
  Ex1:

  ## Examples
      iex> Day23.ex1()
      "27956483"
  """
  def ex1() do
    Shared.read_input_of_day("23")
    |> ex1
  end

  def ex1(input) do
    solve_ex1(input)
  end

  @doc """
  Ex2:

  ## Examples
      iex> Day23.ex2()
      18930983775
  """
  def ex2() do
    Shared.read_input_of_day("23")
    |> ex2
  end

  def ex2(input) do
    solve_ex2(input)
  end

  def measure(function) do
    function
    |> :timer.tc()
    |> elem(0)
    |> Kernel./(1_000_000)
  end

  def main() do
    measure(&ex1/0) |> IO.inspect()
    measure(&ex2/0) |> IO.inspect()
  end
end
