defmodule Day07 do
  @moduledoc """
  Documentation for `Day07`.
  """

  def parse_rule(line) do
    [key, values] = String.split(line, [" bags contain "])

    values =
      values
      |> String.split([" bags, ", " bags.", " bag, ", " bag."], trim: true)
      |> Enum.map(fn inside ->
        [v, k] = String.split(inside, " ", trim: true, parts: 2)
        {k, v}
      end)
      |> Map.new()

    {key, values}
  end

  def solve_ex1(input) do
    tree =
      input
      |> Enum.map(&parse_rule(&1))
      # parent, children
      |> Map.new()

    tree_parents =
      Enum.flat_map(tree, fn {k, v} ->
        # child, parent
        Enum.map(v, fn {k_sub, _} -> {k_sub, k} end)
      end)
      |> Enum.reduce(%{}, fn {child, parent}, acc ->
        list = Map.get(acc, child, [])
        Map.put(acc, child, [parent] ++ list)
      end)

    tree
    |> Enum.reduce([], fn {k, v}, acc ->
      if Map.has_key?(v, "shiny gold") do
        while([], tree_parents, k) ++ acc
      else
        acc
      end
    end)
    |> List.flatten()
    |> Enum.uniq()
    |> Enum.count()
  end

  ## acc is de parents die overlopen zijn, met next_key de bovenstaande parent, die al is toegevoegd
  # bestaat die key als een child? Ja, voeg toe en bekijk alle parents
  def while(acc, tree_parents, next_key) do
    if Map.has_key?(tree_parents, next_key) do
      [next_key] ++
        Enum.map(Map.get(tree_parents, next_key), fn parent ->
          while([], tree_parents, parent)
        end) ++
        acc
    else
      [next_key] ++ acc
    end
  end

  def solve_ex2(input) do
    tree =
      input
      |> Enum.map(&parse_rule(&1))
      # parent, children
      |> Map.new()

    get_sum(tree, "shiny gold")
  end

  def get_sum(tree, key) do
    if Map.has_key?(tree, key) do
      children = Map.get(tree, key)

      Enum.map(children, fn {name, amount} ->
        if amount == "no",
          do: 0,
          else: String.to_integer(amount) + String.to_integer(amount) * get_sum(tree, name)
      end)
      |> Enum.sum()
    else
      1
    end
  end

  #### Clutter
  @doc """
  Ex1:

  ## Examples
      iex> Day07.ex1()
      169
  """
  def ex1() do
    Shared.read_input_of_day_to_list("07")
    |> ex1
  end

  def ex1(input) do
    solve_ex1(input)
  end

  @doc """
  Ex2:

  ## Examples
      iex> Day07.ex2()
      82372
  """
  def ex2() do
    Shared.read_input_of_day_to_list("07")
    |> ex2
  end

  def ex2(input) do
    solve_ex2(input)
  end

  def main do
  end
end
