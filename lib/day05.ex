defmodule Day05 do
  @moduledoc """
  Documentation for `Day05`.
  """

  @doc """
  calculate_coordinates:

  ## Examples
      iex> Day05.calculate_coordinates("FBFBBFFRLR")
      {44,5}
  """
  def calculate_coordinates(seat) do
    <<row::7, column::3>> =
      Enum.into(String.codepoints(seat), <<>>, fn code -> <<code_to_bit(code)::1>> end)

    {0 + row, 0 + column}
  end

  def code_to_bit("F"), do: 0
  def code_to_bit("B"), do: 1
  def code_to_bit("L"), do: 0
  def code_to_bit("R"), do: 1

  def seat_id({row, column}) do
    row * 8 + column
  end

  def solve_ex1(input) do
    input
    |> Enum.map(&(&1 |> calculate_coordinates |> seat_id))
    |> Enum.max()
  end

  def solve_ex2(input) do
    input
    |> Enum.map(&(&1 |> calculate_coordinates |> seat_id))
    |> Enum.sort()
    |> Enum.chunk_every(2, 1, :discard)
    |> Enum.find(fn [a, b] -> b - a == 2 end)
    |> (&(List.first(&1) + 1)).()
  end

  #### Clutter
  @doc """
  Ex1:

  ## Examples
      iex> Day05.ex1()
      888
  """
  def ex1() do
    Shared.read_input_of_day_to_list("05")
    |> ex1
  end

  def ex1(input) do
    solve_ex1(input)
  end

  @doc """
  Ex2:

  ## Examples
      iex> Day05.ex2()
      522
  """
  def ex2() do
    Shared.read_input_of_day_to_list("05")
    |> ex2
  end

  def ex2(input) do
    solve_ex2(input)
  end

  def main do
  end
end
