defmodule Day22 do
  @moduledoc """
  Documentation for `22`.
  """
  def parse_input(input) do
    {p1, p2} =
      input
      |> List.delete_at(0)
      |> List.delete("Player 2:")
      |> Enum.split_while(fn x -> x != "" end)

    {p1 |> Enum.map(&String.to_integer(&1)),
     p2 |> List.delete_at(0) |> Enum.map(&String.to_integer(&1))}
  end

  def solve_ex1(input) do
    {p1, p2} = parse_input(input)
    winner = play_game(p1, p2)
    calculate_score(elem(winner, 1))
  end

  def play_game(p1, p2) do
    if p1 == [] or p2 == [] do
      if p1 == [], do: {:p2, p2}, else: {:p1, p1}
    else
      k1 = Enum.at(p1, 0)
      k2 = Enum.at(p2, 0)

      if k1 > k2 do
        play_game(List.delete_at(p1, 0) ++ [k1] ++ [k2], p2 |> List.delete_at(0))
      else
        play_game(p1 |> List.delete_at(0), List.delete_at(p2, 0) ++ [k2] ++ [k1])
      end
    end
  end

  def calculate_score(cards) do
    cards
    |> Enum.reverse()
    |> Enum.zip(1..Enum.count(cards))
    |> Enum.reduce(0, fn {nr, index}, acc -> acc + nr * index end)
  end

  def play_game_recursive(p1, p2, games \\ []) do
    if [p1, p2] in games do
      {:p1, p1}
    else
      games = games ++ [[p1, p2]]

      if p1 == [] or p2 == [] do
        if p1 == [], do: {:p2, p2}, else: {:p1, p1}
      else
        k1 = Enum.at(p1, 0)
        k2 = Enum.at(p2, 0)

        if Enum.count(p1) - 1 < k1 or Enum.count(p2) - 1 < k2 do
          if k1 > k2 do
            play_game_recursive(
              List.delete_at(p1, 0) ++ [k1] ++ [k2],
              p2 |> List.delete_at(0),
              games
            )
          else
            play_game_recursive(
              p1 |> List.delete_at(0),
              List.delete_at(p2, 0) ++ [k2] ++ [k1],
              games
            )
          end
        else
          # recursive game can happen
          {winner, _} =
            play_game_recursive(
              p1 |> List.delete_at(0) |> Enum.take(k1),
              p2 |> List.delete_at(0) |> Enum.take(k2),
              []
            )

          if winner == :p1 do
            play_game_recursive(
              List.delete_at(p1, 0) ++ [k1] ++ [k2],
              p2 |> List.delete_at(0),
              games
            )
          else
            play_game_recursive(
              p1 |> List.delete_at(0),
              List.delete_at(p2, 0) ++ [k2] ++ [k1],
              games
            )
          end
        end
      end
    end
  end

  def solve_ex2(input) do
    {p1, p2} = parse_input(input)
    winner = play_game_recursive(p1, p2)
    calculate_score(elem(winner, 1))
  end

  #### Clutter
  @doc """
  Ex1:

  ## Examples
      iex> Day22.ex1()
      2556
  """
  def ex1() do
    Shared.read_input_of_day_to_list("22")
    |> ex1
  end

  def ex1(input) do
    solve_ex1(input)
  end

  @doc """
  Ex2:

  ## Examples
      iex> Day22.ex2()
      415579909629976
  """
  def ex2() do
    Shared.read_input_of_day_to_list("22")
    |> ex2
  end

  def ex2(input) do
    solve_ex2(input)
  end

  def measure(function) do
    function
    |> :timer.tc()
    |> elem(0)
    |> Kernel./(1_000_000)
  end

  def main() do
    measure(&ex1/0) |> IO.inspect()
    measure(&ex2/0) |> IO.inspect()
  end
end
